-=-=-=-=-=-=-=-=-=-=-=-=-=- Andrei Bogdan Alexandru --=-=-=-=-=-=-=-=-=-=-=-=-=-
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= 324 CA -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= Tema 2 PC -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

	Intarziere de 3 minute deoarece aveam executabilul selectserver in loc de
server .. Mi-am dat seama dupa.

	Explicatia din README este insotita de comentarii in cod.

	Implementarea temei s-a bazat pe scheletul de cod de TCP Multiplexare din 
cadrul laboratorului pentru a rezolva majoritatea cerintelor, iar pentru cerinta 
 unlock am preluat cod din cel de UDP. Alte cateva functii ajutatoare se gasesc 
in lib.h.

	Userii sunt tinuti in memorie in structuri pe care le populam la inceputul 
executiei lui.

	Astfel ca serverul isi asteapta clientii pe cate un socket inactiv pe care 
il va pastra in cazul in care acesta se conecteaza, fiind introdus in multimea 
clientilor.

	Un client va face si el diferenta print multiplexare intre:
		-> mesajele primite de la tastatura pe care dupa o scurta analiza le va 
		trimite catre server pentru a fi evaluate
		-> legatura UDP
		-> mesajele primite de la server

	In momentul in care o comanda trece testele de sintaxa (sa contina toti 
parametrii) aceasta ajunge la server unde va fi evaluata.
	Serverul va evalua comanda primita si va returna un cod in functie de ce s-a
intamplat. In general codul 0 va insemna succes, un alt numar va reprezenta 
numarul erorii. Acestea sunt interpretate in client, cel care va afisa pe ecran 
si va scrie in fisierul de log creat pentru procesul repsectiv aceste comenzi 
precedate de comenziile care le-au generat.

	In momentul in care cineva isi va bloca un card in ATM, comanda unlock va 
functiona pe calea UDP unde se va trimite numarul cardului blocat (ultimul 
login incercat) urmand ca acesta sa trimita si o parola.

	Fisierele de log create conform cerintei contin in ele comanda actuala, 
si anume orice string citit de la tastatura, urmat de eroarea prezentata in 
cerinta.
	In momentul in care un client tasteaza quit, serverul va fi instiintat cu 
privire la acest lucru si ii va elibera socketul.
	Daca serverul va primi comanda quit atunci el va inchide toti clientii 
trimitand un mesaj corespunzator, dupa aceae se va inchide si pe el insusi.
	
	Cod duplicat din cauza tratarii separate a cazurilor de citire de la 
tastatura si de comunicare cu serverul.
