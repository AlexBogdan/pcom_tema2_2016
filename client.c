#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include "lib.h"

#define BUFLEN 256

void error(char *msg)
{
    perror(msg);
    exit(0);
}

void send_and_wait(int sockfd, char *sendy, char *receivy) {
    int n;
    n = send(sockfd,sendy,strlen(sendy), 0);
    if (n < 0) 
         error("ERROR writing to socket");
    memset(receivy, 0, BUFLEN);
    if ((n = recv(sockfd, receivy, BUFLEN, 0)) <= 0)
        error("ERROR in recv");
    printf("Am primit in [CLIENT] mesajul : %s\n", receivy);
}

int main(int argc, char *argv[])
{
    int sockfd, n, i, j, k;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[BUFLEN];
    if (argc < 3) {
       fprintf(stderr,"Usage %s <IP_server> <port_server>\n", argv[0]);
       exit(0);
    }  
    
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(atoi(argv[2]));
    inet_aton(argv[1], &serv_addr.sin_addr);
    
    
    if (connect(sockfd,(struct sockaddr*) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");    
    
    /**
        Pentru select.
    */
    int fdmax;
    fd_set read_fds, tmp_fds;
    FD_ZERO(&read_fds);
    FD_ZERO(&tmp_fds);
    
    FD_SET(0, &read_fds);  // Adaugam citirea de la tastatura
    FD_SET(sockfd, &read_fds);  // Adaugam socketul serverului
    listen(sockfd, 2);
    fdmax = sockfd;
    int aux;

    /**
        Pentru UDP
    */
    int wait_for_pass = 0;
    int udp_sock, udp_addr_len;
    struct sockaddr_in udp_server_addr;
    struct hostent *udp_host;
    udp_host = (struct hostent *) gethostbyname((char *) argv[1]);
    udp_sock = socket(PF_INET, SOCK_DGRAM, 0);

    udp_server_addr.sin_family = PF_INET;
    udp_server_addr.sin_port = htons(atoi(argv[2]) -1);
    udp_server_addr.sin_addr = *((struct in_addr *)udp_host->h_addr);
    bzero(&(udp_server_addr.sin_zero),8);
    udp_addr_len = sizeof(struct sockaddr);

    FD_SET(udp_sock, &read_fds);
    listen(udp_sock, 2);
    if (fdmax < udp_sock) {
        fdmax = udp_sock;
    }
    /**
        Primim comenziile de la utilizator
    */
    int is_connected = 0;
    char cmdParts[6][16];  // Verificam care este comanda primita
    char *token;
    char temp[BUFLEN];
    int incercari = 0;  // Numarul de incercari la care vom bloca accesul
    char block[BUFLEN];  // Formam mesajul pe care serverul il va recunoaste atunci
                        // cand va trebui sa blocheze un anumit card
    char last_card[7];  // Tinem minte ultimul card incercat la login in caz ca facem unlock

    char filename[32];
    FILE *log;  // Fisierul in care vom scrie logul
    memset(filename, 0, 32);  // Formam numele fisierului de log
    strcpy(filename, "client-");
    snprintf(filename + strlen(filename), sizeof(int), "%d", getpid());
    strcpy(filename + strlen(filename), ".log");
    log = fopen(filename, "a");  // Descriem fisierul de log pentru scriere

    while(1){
        tmp_fds = read_fds; 
        if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) 
            error("ERROR in select");
        if (FD_ISSET(0, &tmp_fds)) {  // citesc de la tastatura

            memset(buffer, 0 , BUFLEN);  // Primim o comanda noua
            fgets(buffer, BUFLEN-1, stdin);
            memcpy(temp, buffer, BUFLEN);
            fprintf(log, "%s", temp);

            // Desfacem comanda in bucati, iar 'i' va ramane numarul de componente
            memset(cmdParts, 0 , sizeof(cmdParts));
            for (i = 0, token = strtok(buffer, " \n"); token != NULL; ++i, token = strtok(NULL, " \n")) {
                memcpy(cmdParts[i], token, 16);
                //printf("%s\n", cmdParts[i]);
            }

            // Verificam daca am primit o comanda valida
            if (is_login(cmdParts[0])) {
                if (i != 3) {
                    printf("−10 : login <numar card> <pin>\n");
                    fprintf(log, "−10 : login <numar card> <pin>\n");
                    continue;
                }
                if (is_connected) {
                    printf("−2 : Sesiune deja deschisa\n");
                    fprintf(log, "−2 : Sesiune deja deschisa\n");
                    continue;
                }

                //trimit mesaj catre server si asteapta raspunsul
                
                strcpy(last_card, cmdParts[1]);
                n = send(sockfd,temp,strlen(temp), 0);
                if (n < 0) 
                     error("ERROR writing to socket");
            } else {
                if (is_unlock(cmdParts[0])) {
                    memset(buffer, 0, BUFLEN);
                    strcpy(buffer, cmdParts[0]);
                    strcpy(buffer+strlen(buffer), " ");
                    strcpy(buffer+strlen(buffer), last_card);
                    printf("%s\n", buffer);
                    sendto(udp_sock, buffer, strlen(buffer), 0, (struct sockaddr *)&udp_server_addr, sizeof(struct sockaddr));
                    wait_for_pass = 1;
                }
                else if (wait_for_pass == 1) {
                    sendto(udp_sock, cmdParts[0], strlen(cmdParts[0]), 0, (struct sockaddr *)&udp_server_addr, sizeof(struct sockaddr));
                    wait_for_pass = 0;
                }

                else if (!is_connected) {
                    printf("−1 : Clientul nu este autentificat\n");
                    fprintf(log, "−1 : Clientul nu este autentificat\n");
                }

                else if (is_logout(cmdParts[0])) {
                    n = send(sockfd,temp,strlen(temp), 0);
                    if (n < 0) 
                         error("ERROR writing to socket");
                }

                else if (is_listsold(cmdParts[0])) {
                    n = send(sockfd,temp,strlen(temp), 0);
                    if (n < 0) 
                         error("ERROR writing to socket");
                }

                else if (is_getmoney(cmdParts[0])) {
                    if (i != 2) {
                        printf("ATM > −10 : getmoney <suma retragere>\n");
                        fprintf(log, "ATM > −10 : getmoney <suma retragere>\n");
                        continue;
                    }
                    if (atoi(cmdParts[1]) % 10 != 0) {
                        printf("ATM > −9 : Suma nu este multiplu de 10>\n");
                        fprintf(log, "ATM > −9 : Suma nu este multiplu de 10>\n");
                        continue;
                    }

                    n = send(sockfd,temp,strlen(temp), 0);
                    if (n < 0) 
                         error("ERROR writing to socket");
                }

                else if (is_putmoney(cmdParts[0])) {
                    if (i != 2) {
                        printf("ATM > −10 : putmoney <suma depunere>\n");
                        fprintf(log, "ATM > −10 : putmoney <suma depunere>\n");
                        continue;
                    }

                    n = send(sockfd,temp,strlen(temp), 0);
                    if (n < 0) 
                         error("ERROR writing to socket");
                }
            }

            if (is_quit(cmdParts[0])) {
                n = send(sockfd,temp,strlen(temp), 0);
                if (n < 0) 
                     error("ERROR writing to socket");
            }
        }

        if (FD_ISSET(udp_sock, &tmp_fds)) {
            memset(buffer, 0, BUFLEN);
            recvfrom(udp_sock,buffer, BUFLEN-1,0,(struct sockaddr *)&udp_server_addr, &udp_addr_len);
            if (! strcmp(buffer, "u0")) {
                printf("UNLOCK > Trimite parola secreta\n");
                fprintf(log, "UNLOCK > Trimite parola secreta\n");
            }
            else if (! strcmp(buffer, "u00")) {
                printf("UNLOCK > Client deblocat\n");
                fprintf(log, "UNLOCK > Client deblocat\n");
            }
            else if (! strcmp(buffer, "u7")) {
                printf("UNLOCK > -7 : Deblocare esuata\n");
                fprintf(log, "UNLOCK > -7 : Deblocare esuata\n");
            }
            else {
                printf("EROARE IN UDP PE CLIENT\n");
            }
        }

        // Interpretam in client raspunsul primit de la server
        if (FD_ISSET(sockfd, &tmp_fds)) {
            //  Primim mesajul de la server
            memset(buffer, 0, BUFLEN);
            if ((n = recv(sockfd, buffer, BUFLEN-1, 0)) <= 0)
                error("ERROR in recv");

            // Interpretam mesajul in functie de ultima comanda pe care
            // o avem salvata in cmdParts
            if (is_login(cmdParts[0])) {
                if (! strcmp(buffer, "2")) {
                    printf("ATM> −2 : Sesiune deja deschisa\n");
                    fprintf(log, "ATM> −2 : Sesiune deja deschisa\n");
                }
                else if (! strcmp(buffer, "3")) {
                    incercari++;
                    if (incercari == 3) {
                        goto block_card;
                    }
                    printf("ATM> −3 : Pin gresit\n");
                    fprintf(log, "ATM> −3 : Pin gresit\n");
                }
                else if (! strcmp(buffer, "4")) {
                    incercari++;
                    if (incercari == 3) {
                        goto block_card;
                    }
                    printf("ATM> −4 : Numar card inexistent\n");
                    fprintf(log, "ATM> −4 : Numar card inexistent\n");
                }
                else if (! strcmp(buffer, "5")) {
                    printf("ATM> -5 : Card blocat\n");
                    fprintf(log, "ATM> -5 : Card blocat\n");
                }
                else {
                    printf("ATM> %s\n", buffer);
                    fprintf(log, "ATM> %s\n", buffer);
                    is_connected = 1;
                }
block_card:  // Blocam cardul la 3 incercari
                if (incercari == 3) {
                    memset(block, 0 , BUFLEN);
                    strcpy(block, "block ");
                    strcpy(block + 6, cmdParts[1]);
                    send_and_wait(sockfd, block, buffer);
                    if (! strcmp(buffer, "0")) {
                        printf("ATM> -5 : Card blocat\n");
                        fprintf(log, "ATM> -5 : Card blocat\n");
                        incercari = 0;
                    } else {
                        printf("ATM> EROARE BLOCARE\n");
                    }
                }
            } else {
                if (is_logout(cmdParts[0])) {
                    if (! strcmp(buffer, "0")) {
                        printf("ATM> Deconectare de la bancomat\n");
                        fprintf(log, "ATM> Deconectare de la bancomat\n");
                        is_connected = 0;
                    }
                    else {
                        printf("ATM> EROARE LOGOUT\n");
                    }
                }

                else if (is_listsold(cmdParts[0])) {
                    if (strlen(buffer) > 0) {
                        printf("ATM > %s\n", buffer);
                        fprintf(log, "ATM > %s\n", buffer);
                    } else {
                        printf("ATM> EROARE LIST_SOLD\n");
                    }
                }

                else if (is_getmoney(cmdParts[0])) {
                    if (! strcmp(buffer, "8")) {
                        printf("ATM> −8 : Fonduri insuficiente\n");
                        fprintf(log, "ATM> −8 : Fonduri insuficiente\n");
                    }
                    else if (! strcmp(buffer, "0")) {
                        printf("ATM> Suma %s retrasa cu succes\n", cmdParts[1]);
                        fprintf(log, "ATM> Suma %s retrasa cu succes\n", cmdParts[1]);
                    }
                    else {
                        printf("ATM> EROARE GET_MONEY\n");
                    }
                }

                else if (is_putmoney(cmdParts[0])) {
                    if (! strcmp(buffer, "0")) {
                        printf("ATM> Suma depusa cu succes\n");
                        fprintf(log, "ATM> Suma depusa cu succes\n");
                    }
                    else {
                        printf("ATM> EROARE PUT_MONEY\n");
                    }
                }

                else if (is_unlock(cmdParts[0])) {
                    
                }
            }

            if (is_quit(cmdParts[0])) {
                if (! strcmp(buffer, "0")) {
                    printf("Proces incheiat cu succes\n");
                    fprintf(log, "Proces incheiat cu succes\n");
                    return 0;
                }
                else {
                    printf("ATM> EROARE QUIT\n");
                }
            }

            if (! strcmp(buffer, "Serverul se va inchide.")) {
                printf("Serverul a fost inchis.\n");
                return 0;
                //fprintf(log, "Serverul a fost inchis.\n");
            }
        }
    }

    return 0;
}
