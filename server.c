#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "lib.h"

#define MAX_CLIENTS	5
#define BUFLEN 256

void error(char *msg)
{
    perror(msg);
    exit(1);
}


/**
	Structura in care vom tine minte datele unui user
*/
typedef struct User{
	int id;
	int is_connected;
	int is_blocked;
	char nume[12];
	char prenume[12];
	unsigned long int numar_card;
	unsigned short int pin;
	char parola[16];
	float sold;
}User;

/**
	Constructor pentru structua User de mai sus
*/
User read_user_data(FILE *users_data_file) {
	User user;
	user.is_connected = 0;
	user.is_blocked = 0;
	fscanf(users_data_file, "%s %s %lu %hu %s %f", 
		&user.nume,
		&user.prenume,
		&user.numar_card,
		&user.pin,
		&user.parola,
		&user.sold);

	return user;
}
/**
	Mai mult pentru Debug. Afisam un user
*/
void printUser(User *user) { 
	printf("Nume : %s\nPrenume : %s\nNumar card = %lu\nPIN = %u\nParola = %s\nSold %.02f\n\n",
		user->nume, user->prenume, user->numar_card, user->pin, user->parola, user->sold);
}

/**
	Verificam daca numarul de card primit corespunde cu unul dintre cardurile existente
*/
int search_for_card(User *users, int card, int size) {
	int i;
	for (i = 0 ; i < size ; ++i) {
		if (users[i].numar_card == card) {
			return i;
		}
	}
	return -1;
}

User* get_user_by_id(User *users, int index, int size) {
	int i;
	for (i = 0; i < size ; ++i) {
		if (users[i].id == index) {
			printUser(&users[i]);
			return &users[i];
		}
	}
	return NULL;
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno, clilen;
     char buffer[BUFLEN];
     struct sockaddr_in serv_addr, cli_addr;
     int n, i, j;

     fd_set read_fds;	//multimea de citire folosita in select()
     fd_set tmp_fds;	//multime folosita temporar 
     int fdmax;		//valoare maxima file descriptor din multimea read_fds

     if (argc < 3) {
         fprintf(stderr,"Usage : %s <port_server> <users_data_file>\n", argv[0]);
         exit(1);
     }

/*
	Citim fisierul ce contine datele userilor folosind structura User de mai sus; 
*/
     FILE *users_data_file = fopen(argv[2] , "r");
     int N;
     fscanf(users_data_file, "%d", &N);
     User users[N];
     for (i = 0 ; i < N ; ++i) {
     	users[i] = read_user_data(users_data_file);
     	//printUser(&users[i]);
     }
     close(users_data_file);


     //golim multimea de descriptori de citire (read_fds) si multimea tmp_fds 
     FD_ZERO(&read_fds);
     FD_ZERO(&tmp_fds);
     
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     
     portno = atoi(argv[1]);

     memset((char *) &serv_addr, 0, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;	// foloseste adresa IP a masinii
     serv_addr.sin_port = htons(portno);
     
     if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr)) < 0) 
              error("ERROR on binding");
     
     listen(sockfd, MAX_CLIENTS);

     //adaugam noul file descriptor (socketul pe care se asculta conexiuni) in multimea read_fds
     FD_SET(0, &read_fds);
     FD_SET(sockfd, &read_fds);
     fdmax = sockfd;


    /**
        Pentru UDP
    */
    int wait_for_pass = 0;
    int udp_sock, udp_addr_len;
    struct sockaddr_in udp_server_addr, udp_client_addr;
    struct hostent *udp_host;
    udp_sock = socket(PF_INET, SOCK_DGRAM, 0);

    udp_server_addr.sin_family = PF_INET;
    udp_server_addr.sin_port = htons(atoi(argv[1]) -1);
    udp_server_addr.sin_addr.s_addr = INADDR_ANY;
    bzero(&(udp_server_addr.sin_zero),8);

    if (bind(udp_sock,(struct sockaddr *)&udp_server_addr, sizeof(struct sockaddr)) == -1){
		perror("Bind");
		exit(1);
	}
	udp_addr_len = sizeof(struct sockaddr);
	FD_SET(udp_sock, &read_fds);
	

     // main loop
    char cmdParts[6][16];
    char *token;
    int card;
    int index_user;
    float sold;
    char welcome[35];
    char filename[32];
    FILE *log;  // Fisierul in care vom scrie logul
    int last_sent_card;
	while (1) {
		tmp_fds = read_fds; 
		if (select(fdmax + 1, &tmp_fds, NULL, NULL, NULL) == -1) 
			error("ERROR in select");
		
		if (FD_ISSET(0, &tmp_fds)) {
			memset(buffer, 0 , BUFLEN);  // Primim o comanda noua
            fgets(buffer, BUFLEN-1, stdin);
            
            if (! strcmp(buffer, "quit\n")) {
            	memset(buffer, 0, BUFLEN-1);
            	strcpy(buffer, "Serverul se va inchide.");
                for (j = 1; j <= fdmax; j++) {
                	if (FD_ISSET(j, &read_fds) && j != udp_sock && j != sockfd) {
                		printf("%s\n", buffer);
                		n = send(j,buffer,strlen(buffer), 0);
		                if (n < 0) 
		                     error("ERROR writing to socket");
                	}
                }
                return 0;
            } else {
            	printf("−10 : Se accepta doar quit\n");
            }
		}
	
		for(i = 1; i <= fdmax; i++) {
			if (FD_ISSET(i, &tmp_fds)) {
				if (i == sockfd) {
					// a venit ceva pe socketul inactiv(cel cu listen) = o noua conexiune
					// actiunea serverului: accept()
					clilen = sizeof(cli_addr);
					if ((newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen)) == -1) {
						error("ERROR in accept");
					} 
					else {
						//adaug noul socket intors de accept() la multimea descriptorilor de citire

						FD_SET(newsockfd, &read_fds);
						if (newsockfd > fdmax) { 
							fdmax = newsockfd;
						}
					}
					printf("Noua conexiune de la %s, port %d, socket_client %d\n ", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port), newsockfd);
				}
				
				else if (i == udp_sock) {
					memset(buffer, 0, BUFLEN-1);
					recvfrom(udp_sock,buffer, BUFLEN-1, 0,(struct sockaddr *)&udp_client_addr, &udp_addr_len);

					memset(cmdParts, 0, sizeof(cmdParts));
					for (j = 0, token = strtok(buffer, " \n"); token != NULL; ++j, token = strtok(NULL, " \n")) {
			            memcpy(cmdParts[j], token, 16);
			        }  // Desfacem comanda in bucati, iar 'j' va ramane numarul de componente

					if (! strcmp(cmdParts[0], "unlock") && wait_for_pass == 0) {  // Asteptam parola
						last_sent_card = atoi(cmdParts[1]);
						strcpy(buffer, "UNLOCK > Trimite parola secreta");
						sendto(udp_sock, "u0", 2, 0, (struct sockaddr *)&udp_client_addr, sizeof(struct sockaddr));
						wait_for_pass = 1;
					}
					else if (wait_for_pass == 1) {
						index_user = search_for_card(&users, last_sent_card, N);
						if (last_sent_card >= 0) {
							if (! strcmp(users[index_user].parola, buffer)) {
								users[index_user].is_blocked = 0;
								strcpy(buffer, "UNLOCK > Client deblocat");
								sendto(udp_sock, "u00", 3, 0, (struct sockaddr *)&udp_client_addr, sizeof(struct sockaddr));
							} else {
								strcpy(buffer, "UNLOCK > -7 : Deblocare esuata");
								sendto(udp_sock, "u7", 2, 0, (struct sockaddr *)&udp_client_addr, sizeof(struct sockaddr));
							}
						} else {
							printf("Eroare ciudata in UDP\n");
						}
						wait_for_pass = 0;
						last_sent_card = -1;
					}
					else {
						printf("EROARE UDP\n");
					}
				}

				else {
					// am primit date pe unul din socketii cu care vorbesc cu clientii
					//actiunea serverului: recv()
					memset(filename, 0, 32);  // Formam numele fisierului de log
					strcpy(filename, "client-");
					//snprintf(filename +7, "%d", i);
					strcpy(filename + strlen(filename), ".log");
					log = fopen(filename, "a");  // Descriem fisierul de log pentru scriere

					memset(buffer, 0, BUFLEN);
					if ((n = recv(i, buffer, BUFLEN-1, 0)) <= 0) {
						if (n == 0) {
							//conexiunea s-a inchis
							printf("selectserver: socket %d hung up\n", i);
						} else {
							error("ERROR in recv");
						}
						close(i); 
						FD_CLR(i, &read_fds); // scoatem din multimea de citire socketul pe care 
					} 
					
					else { //recv intoarce >0
						printf ("Am primit de la clientul de pe socketul %d, mesajul: %s\n", i, buffer);
						// Scriem de acum comanda primita pentru ca mai jos o vom distruge ..
						memset(cmdParts, 0, sizeof(cmdParts));
						for (j = 0, token = strtok(buffer, " \n"); token != NULL; ++j, token = strtok(NULL, " \n")) {
				            memcpy(cmdParts[j], token, 16);
				        }  // Desfacem comanda in bucati, iar 'j' va ramane numarul de componente

				        // Verificam comanda primita
				        if (is_login(cmdParts[0])) {
				            
				            // Intoarce indexul userului cu cardul respectiv
				            index_user = search_for_card(&users, atoi(cmdParts[1]), N);
				            last_sent_card = atoi(cmdParts[1]);
				        	if (index_user >= 0) {
				        		if (! users[index_user].is_blocked) {
				        			if (atoi(cmdParts[2]) == users[index_user].pin) {
					        			if (!users[index_user].is_connected) {
					        				// Formam mesajul de welcome
					        				memset(welcome, 0, sizeof(welcome));
					        				strcpy(welcome, "Welcome ");
					        				strcpy(welcome + 8, users[index_user].nume);
					        				welcome[8 + strlen(users[index_user].nume)] = ' ';
					        				strcpy(welcome + 9 + strlen(users[index_user].nume), users[index_user].prenume);
					        				n = send(i,welcome,strlen(welcome), 0); // Autentificare acceptata
					        				if (n < 0)
						                 		error("ERROR writing to socket");
					        				else {
					        					users[index_user].is_connected = 1;
					        					users[index_user].id = i;
						                 	}
					        			} else {
					        				n = send(i,"2",1, 0);  // Sesiune deja pornita
					        				if (n < 0)
						                 		error("ERROR writing to socket");
					        			}
					        		} else {
					        			n = send(i,"3",1, 0);  // Pin gresit
					        			if (n < 0)
					                 		error("ERROR writing to socket");
					        		}
				        		} else {
				        			n = send(i,"5",1,0);  // Card blocat
				        			if (n < 0)
				        				error("ERROR writing to socket");
				        		}
				        	} else {
				        		n = send(i,"4",1, 0);  // Card inexistent
				        		if (n < 0)
			                 		error("ERROR writing to socket");
				        	}

				        } else {
				            if (is_logout(cmdParts[0])) {
				            	(get_user_by_id(&users, i, N))->is_connected = 0;
				            	(get_user_by_id(&users, i, N))->id = -1;
				            	n = send(i,"0",1, 0);  // Sesiunea pentru acest card a fost incheiata
				                if (n < 0)
			                 		error("ERROR writing to socket");
				            }

				            else if (is_listsold(cmdParts[0])) {
				            	memset(buffer, 0, sizeof(buffer));
				            	sold = (get_user_by_id(&users, i, N))->sold;
				                snprintf(buffer, sizeof(buffer), "%.02f", sold);
				                printf("LISTSOLD : %s\n", buffer);
				                n = send(i, buffer, strlen(buffer), 0);  // Trimitem info despre sold
				                if (n < 0)
			                 		error("ERROR writing to socket");
				            }

				            else if (is_getmoney(cmdParts[0])) {
				            	if ((get_user_by_id(&users, i, N))->sold >= atoi(cmdParts[1])) {
					                (get_user_by_id(&users, i, N))->sold -= atoi(cmdParts[1]);
									
					                n = send(i,"0",1, 0);  // Suma a fost retrasa cu succes
					                if (n < 0)
				                 		error("ERROR writing to socket");
				                } else {
				                	n = send(i,"8",1, 0);  // Fonduri insuficiente
					                if (n < 0)
				                 		error("ERROR writing to socket");
				                }
				            }

				            else if (is_putmoney(cmdParts[0])) {
				                (get_user_by_id(&users, i, N))->sold += atof(cmdParts[1]);
				                n = send(i,"0",1, 0);  // Suma a fost depusa cu succes
				                if (n < 0)
			                 		error("ERROR writing to socket");
				            }

				            else if (is_blocked(cmdParts[0])) {
				            	index_user = search_for_card(&users, atoi(cmdParts[1]), N);
				            	if (index_user > 0) {
				            		users[index_user].is_blocked = 1;
				            		n = send(i,"0",1, 0);  // Cardul a fost blocat cu succes
					                if (n < 0)
				                 		error("ERROR writing to socket");
				            	} else {
				            		printf("EROARE CIUDATA IN SERVER LA BLOCARE\n");
				            	}
				            }

				            else if (is_quit(cmdParts[0])) {
				            	if ((get_user_by_id(&users, i, N)) != NULL) {
				            		(get_user_by_id(&users, i, N))->is_connected = 0;
				            		(get_user_by_id(&users, i, N))->id = -1; // Deconectam fortat cardul
				            	}
				            	n = send(i,"0",1, 0);  // Canalul a fost inchis
				                if (n < 0)
			                 		error("ERROR writing to socket");
			                 	else {
			                 		close(i);
									FD_CLR(i, &read_fds); // Inchidem canalul cu procesul
			                 	}
				            }
				        }
					}
				} 
			}
		}
     }
     close(sockfd);
   
     return 0; 
}


