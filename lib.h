#include <string.h>


// Desfacem comanda in bucati, iar 'i' va ramane numarul de componente
void cmdtok(char *buffer, char **cmdParts) {
	char *token;
	int i; 
	printf("Ceva\n");
	for (i = 0, token = strtok(buffer, " \n"); token != NULL; ++i, token = strtok(NULL, " \n")) {
	    memcpy(cmdParts[i], token, sizeof(token));
	    printf("%s\n", cmdParts[i]);
	}
}

int is_login(char *cmd) {
	return (! strcmp(cmd, "login"));
}

int is_logout(char *cmd) {
	return (! strcmp(cmd, "logout"));
}

int is_listsold(char *cmd) {
	return (! strcmp(cmd, "listsold"));
}

int is_getmoney(char *cmd) {
	return (! strcmp(cmd, "getmoney"));
}

int is_putmoney (char *cmd) {
	return (! strcmp(cmd, "putmoney"));
}

int is_unlock(char *cmd) {
	return (! strcmp(cmd, "unlock"));
}

int is_quit(char *cmd) {
	return (! strcmp(cmd, "quit"));
}

int is_blocked(char *cmd) {
	return (! strcmp(cmd, "block"));
}
